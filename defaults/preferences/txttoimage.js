pref('txttoimage.disabled', false);
pref('txttoimage.filter', '');
pref('txttoimage.Height', true);
pref('txttoimage.HeightValue', 200);
pref('txttoimage.HoverH', true);
pref('txttoimage.HoverHV', 350);
pref('txttoimage.HoverW', true);
pref('txttoimage.HoverWV', 350);
pref('txttoimage.imageLoader', false);
pref('txttoimage.imageTypes', 'bmp dib gif jfif jpe jpeg jpg png');
pref('txttoimage.maxNumberOfImages', 50);
pref('txttoimage.mode', 'new');
pref('txttoimage.noGameFAQsSig', true);
pref('txttoimage.siteFilter', 'images.google.com images.search.yahoo.com');
pref('txttoimage.undoOnDisable', true);
pref('txttoimage.Width', true);
pref('txttoimage.WidthValue', 200);
