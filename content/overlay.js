/**
 * Overlay JS
 * Provides the main functions for replacing the image links based on user's
 * preferences provided.
 */

var TxttoImage = new function()
{
  // TODO: don't hardcode this
  // we can't load chrome from unprivileged pages (well, sort of...)
  var txttoimageLoaderSrc = 'data:image/gif;base64,R0lGODlhJgAmALMPAMz%2FzMzMzJn%2FmZnMmZmZmWbMZmaZZmZmZjPMMzOZMzNmMzMzMwBmAAAzAAAAAP%2F%2F%2FyH%2FC05FVFNDQVBFMi4wAwEAAAAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F8cIA4kAHbAUKxIgqzCmQFrkig4zhimXNEtRUNhCBgDPd%2BEZsMRkEoM8KZIRi%2BDpiJwzQwUzURXeiu0CqSSNfo1mF%2B2eGHtMwyY1LxtHqWJgkRHKQYJfD4BAw8CVIkWWQVKKg9ZCo1YCZYgkgAJBhQDdksrdBwkDzSWXw4OqQiZHpKnPQBCqwcTAZ0yAwhrAKurWyacnicAoRS%2FwFtzx2K7hhMGwHdFxwo%2BnJAVBqG01cW7mHQiBQkAAWhKQGgi7ipEKdEyAC0vK%2Fg3mMh95nH%2FYAqNOUXCABE3%2Bs4NxIVEhb5XCycxipjMHA6IY%2BRNXJiCBKAiFA8fdetGqksKd%2BgoqlyJIQIAIfkECQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzbv%2FYCiO5AOcwwmUGDAUcFykrOQWCJIkSr%2FDg5KLt%2BM1ekhForASDXc6hmIQoA4IA2Wi%2BXlZcbugBWBQFECARNCVQxQE3EoVVDCsBsXfu%2FYYGNY8ST80JTASWQp%2FA35aYiMACGIGdhV%2BS3FoaoeYNgaaIwEGATYUAKM2O5wdaZgABTwEE3inIaylBg0NDgunaY5okRQBDsTEvZ%2B1blwDxQ4KTWS%2FIDmOAAvEC4QBMySvM014U3MPr9Jdg9AmLyY7tCJ4CW4pKFNDBjVZRTBhAAI87iS25Ulkqo65DS44uXrVY4YiJ0vmqXgRaMcfVate5SmCRA1GEKEWkvSYRIiPhAAuFp0wybKly5cwY36IAAAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F9gKI5kaZ4YoAIoFgzFYMyxoKKAHA9DoiQJRKHGGsEMg2KugAD%2BEsMiaCBUwobAhkILjIEAUUkPCGzyBgrGryDlyAJin2IOZUsCBrqdA0gMJjAvBAdrbQR5CH8cBQlSbS%2BFFHhQfEIaAAQ%2FAhVoCG0XVIoUN3cGlBUHjRtMBZKHSEU9rRV4nxVgUEpbDg0Lin2ztLdtAKZ7Aw7JyQQSwLYWMG09iSwEyg6%2BzacbAX6SkQG7B8zaoqt%2BSqboKgQ8UozPF325zabUKu715m5zsAKMQAwUEKjoCpwpeRjUkaMASZIcQJKIwJNmjkUDAfCNMRCPz4s8dB78HGko8QSAF1eGnDnYYgKLFS1jypxJs6bNmzg%2FRAAAIfkECQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzbv%2FYCiOZGmeaKpiABC8AbBKwFAghWEUxSCngVwvMCgWDYNTLdH7UQKGhMGZGQQsgFvBWSsKBAWFYrvhUbOI5CQrTjB57oJAw6NkmefioY1wx%2BcYO04DCWkYUAp%2BCQwMeIEJag%2BECIAXAXtvBgqNZBYGDJ1RnZJWg4k%2BNWKGFgOJgKIyAAQLCwpTElCQsZ%2BOFQCaW3e3AwsOxg23v72tY5EUzHIFhTIExta3uZG%2FCqu%2BvDeFqArWC2rKzq2Qh5qJiU0DBDo%2BEoSjDwBxGYhiYjsCqBSiOJMgUEMuNwN0NFlTj4qERA6xSCsQAyCNhhYCjImIJaERVEUgJnJ80OMDETiKFrII0SKhDiQjUcScQbOmzZs4c%2BpcEQEAIfkECQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzbv%2FYCiOGDAA5FcUaMoFyelyRtLOWaAYtwQEg2CwIBMNGKxJoIBoJhIrKLHXGSgKk0ENMQV4vdrpx3CVBLZFy2A1GCNRBgaU%2BvjWg3QMQS4YPJMVAAVzd2kaVlJPbRdnVyd%2Bixs6Ck%2BEQARpVlcCBTt5gWSUcwAHDg4LPGYHciuUAVUKCqxeDaankWQJCJWRGjAJsSwBC7YNqQ%2B5u3%2BfFIIxWkEExA7GPjVRcgjMFGePBkIHO68POnNWcr0ZgicBQCh2dbkCdXuEL2yffjEtR%2FYc%2Bid6mFg2gQAlQBxM1IAiRN%2B3HrkQdgDyZ%2BEKOrkMeQAyoJ2XCwAgyBDBkYMgyQsEdKU7aYaSRpbIYsC8gGemzZs4c%2BrMEAEAIfkECQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzTkYA9CN1lAUIakGRoKK6ggYSgLH41APOMkqhVtvQ2DYJoDk8DIDigKmE0g4DNSCg1Zii%2BimljpXYZtAoEDjVw9QUNTIBYHy8SkYULhmzfytfMwBMSwJbnFUfmokMwY0QR8EfRRZPD4GIAkhAwsOCwYwHyJsQT4FASwhBw6qCpQzCYEmlB1sA1AoBqoODbKNSSCKJ74DBJu6vAyOvzItsjMLDQqBDwEHRgKTKgMJnkjDsjoKZm2yIy2jFz9lY6yCzFQAgy5kmHknZ%2FBZbmLzh7Mm83BCzDBybg0aewLpGCDYL0YoIQPpLdmggwECARM1WGGYEUM6PB0fL4RBECnkNG1lSnYEQOBNIpMSUN6DSSHJHJo4c06IAAAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F8dEIgAAHrAYCiJkRTmmaUr8w54LFupYhcC3e7SUxgEw00RmNSkEixgYCpsPgZQFqJlNAyqO0ABakuYzy8wKFUYR9vwLSwpHgRc35J%2BUEDMZWxTXnoVYgkDO3cwKl%2BMQmJ%2FIAEKMAEFAwQLDgsEFZZfJwFpKQMHDqcKARVYiJItMQGapweqFAB%2BajNmMQAKpw0GvDFtuRhiCLUPBgsNhxJsEnzFGJZ%2FNK0Pogkm0jKsuQQsJsSAY6C24QwwkNMzLl52ASo%2BozhJlmcK%2BvoJCDg5Tdic8YPATwEDXqw8E8CQ4RgzRtqFWXEmmUIKA%2Bhduljojg1%2FEiklzWPAAKQVEQSmzONn0sodflDOAAnJgc1DOHk4TgAgYECQEjqDCtUQAQAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F9gKI7kBJylBgTDYLwvEaTmUBhKzjAKbwyAEsCWyCkShUIryZyFVrZCgocUBGvKp601mB4RQAsK1FUGCMeEOvGjBQwJoEspIDLi11EXGFCe%2FgMIagMkAHCEQwMBixJDUgV5IABIVwNoCwpOQwgIkR8AWRIDCw6lBiYFYCNvhBIGpaYUgQUjk7QPAAewCwQUAAi3TwaQogsNCgaRoMGSUq2JYRVJJGV5yldJnh6gSE6yxMvaHgFqBjInLGozoK21XUUwRskPNt7uUkXyQayfYxUAAlKpKWAFlAF7G94cvLAJGBBH7TpMQqYowJUha278aCFOAxE2MCoI4Nih5kg0EQFL8thxZ83Bjh7qCFwDzA9MSdBaFKSB4Q%2FPn0CDCh0KIgIAIfkECQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzbv%2FYCiOZGmeV6AObAC8qAQMhlEMRZHUxuCaAUMicQvQFMhkDzACCG%2BAlYHBUAwVVUIgBNAVmLPacJg7KgzbD27AfLAGAhyCKHgNEgy0p%2FudGNkvOgkDE12DHgFEbQEEBwcEL4YKaQ98lBp3hJUGCw4ODQSVAkOaMlAcOpoDnZ4OBqY7bZUsHZkSAw2tDZozQ5cWM79uhw9BCgtnsgMICLIXTge%2Fd3qzPs7LfRlOCqW3CQqhmAjdGNOXhksYXc0cdzuXAoJaLioTthySB2wyd0pCpTmcaYv3DRw9HN%2BoWElzr8MMHVQOGLCSgFmZNgAQZNtDkIpCIgUu6oDBcUOEnQJzxuwAhMOGsA8v4uQo2IPkvhgv%2FvhgITCGz59AgwodSrSoURIRAAAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F9gKI5kaZ6oCQRDOwApBQxFYitJ8X4rfM2FWjDo2nEEhsElUHsBnqsAU%2BfTDBQGC8CQMD5WLm4uoAEUFISKOVf90gwGBUOBJWPWSpmtyirG53QGdloICWkTawUSWwoLdQACNTYJdT81gohNEgMLDp4LBE%2BSQYGWlIdflHkDDZ6eB08DNi8Fc4OIsgyYqQp5AQatDqBuNmSMqBQCCHR5eLgGBzszlHZXCreLNXMDAZEJu4tVawl2ANSmgAWFlaYGVbJ5Ft5zCYVzSU8VA4WKEwNJGbzZCEIp0C0zCAq0gadBoI43XcSpU1iBCTYtcSJCoUCDzY8OMyaSBIDyhIkBKifA0BiiDoEXFGBY6oihhiTNmzhz6tzJs6fPnxciAAAh%2BQQJBQAPACwAAAAAJgAmAAAE%2F%2FDJSau9OOvNu%2F9gKI4aAJAfUBgByqkK65ZFogxzBgxJgucYVUIWNJ1GgkKsVdkVntDB0bOzHZiSnaEH7Q0D002VYZgOaomC1Lga%2Fl488kTos5hWhbAubsjybhdGA4N6GAIJDFcmNmUSAYNggwOPBVg0DGRnPUwDCgsLBwRnBoOkhXY1DGkIXxIDDQ6xBwNJBlukHTw9rDFHnbEOC5GICj6nTV6pCkwABrDBYIcK041iBjFP028B16EmAqw2dRydpHxYAGBZaDWAL7Ymf3l2ugUCBmTHFY9g6YSoqsAUyNfBiRQd7Qi4IlgQj6UH8dAQKaePXhRJTxCwqjSBYghKCCdsFUDw5N%2BEgQpHGOlnAlWCh0AoJJkX0yXMmhBN4mxScafPn0BrRgAAIfkEBQUADwAsAAAAACYAJgAABP%2FwyUmrvTjrzTsAwwB0JAUWSWIEZQmgiaIQbMsJsDHU9vbGikHPMwAahp4Cg7FCakaCBEPBc2JAMYLpw7U%2BXkvtF1coGAwFkROAWBo%2BsJQ8kR71iik0SnEIAQIhZQVVJAMKKQhyTVsoiyQABocFiTOEEz9vLgZ0KFM1IAY0l5FCjwMrUUwThg4Li1iZhSIAUmIvDQ6tBH4DZXYcILNKmZC4Dg06aYBqhYMPRQV2Awu5CwQvK34lAWgjIHaQCwsHAaCBv8CnpRV%2FO18Fh90tAb5PqQx06B0DCHUXJ0AURLNxAs0sLjjmyFhHsBcnQYmYlFlo5QSCRHJovFDgaE0gZRIqIDHRVxGdoSBenhhgYillSHgJWrrEgtKlBWwJmNm8hMLZTnbafgodKiECADs%3D';
  var txttoimagePrefsClass = Cc['@mozilla.org/preferences-service;1']
    .getService(Ci.nsIPrefService)
    .getBranch('txttoimage.');
  var txttoimageDisabled           = false;
  var txttoimageImageLoader        = false;
  var txttoimageRegular            = new Array(2);
  var txttoimageRegularv           = new Array(2);
  var txttoimageMouse              = new Array(2);
  var txttoimageMousev             = new Array(2);
  var txttoimageBlacklist          = [];
  var txttoimageSiteBlacklist      = [];
  var txttoimageValidDocTypes      = new RegExp('^(text/html|application/xhtml\\+xml|text/xml|application/xml)$', 'i');
  var txttoimagegContext           = this;
  var txttoimageImgmode, txttoimageStrings, txttoimageImgRegExp, txttoimageImgReplaceRegExp, txttoimageImgDetectRegExp, txttoimageMaxImages, txttoimageImgsonpage, txttoimageCurrImg;
  var txttoimageNoGameFAQsSig      = false;
  var txttoimageNoGameFAQsSigCond  = false;
  var txttoimageUndoOnDisable      = false;

  /**
   * Preference observer
   */
  var txttoimagePrefObserver = {
    register: function()
    {
      this._branch = Cc['@mozilla.org/preferences-service;1']
        .getService(Ci.nsIPrefService)
        .getBranch('txttoimage.');

      this._branch.QueryInterface(Ci.nsIPrefBranch2);

      this._branch.addObserver('', this, false);
    },

    unregister: function()
    {
      if (!this._branch) {
        return;
      }

      this._branch.removeObserver('', this);
    },

    observe: function(aSubject, aTopic, aData)
    {
      if (aTopic != 'nsPref:changed') {
        return;
      }

      txttoimagePrefs();
    }
  };

  /**
   * extension initiation: doing some configuration stuff
   */
  this.Init = function()
  {
    txttoimagePrefObserver.register();
    txttoimagePrefs();
    this.removeEventListener('load', TxttoImage.Init, false);
    Image.prototype._txttoimageData = new txttoimageData();
    Image.constructor = function() { this._txttoimageData = new txttoimageData(); };
    //HTMLImageElement.prototype._txttoimageData = new txttoimageData();
    //HTMLImageElement.constructor = function() { this._txttoimageData = new txttoimageData(); };
    XMLHttpRequest.prototype._txttoimageData = null;
    txttoimageStrings = document.getElementById('txttoimageStringBundle');

    if (!txttoimageDisabled) {
      document.getElementById('txttoimagePanel').setAttribute('disabled', 'true');
    }

    document.getElementById('contentAreaContextMenu').addEventListener('popupshowing', txttoimagePopup, false);
    window.addEventListener('DOMContentLoaded', txttoimageCheckImageLoad, false);
    window.addEventListener('blur', txttoimageWindowBlur, false);
  }

  /**
   * Opens the options menu, simple enough (now focuses the current active
   * window if it's already open)
   */
  function txttoimageOpen()
  {
    var prefWindow = Cc['@mozilla.org/appshell/window-mediator;1']
      .getService(Ci.nsIWindowMediator)
      .getEnumerator('txttoimage:options');

    if (prefWindow && prefWindow.focus)
      prefWindow.focus();
    else
      window.open('chrome://txttoimage/content/options.xul', '', 'chrome, resizable, centerscreen');
  }

  this.Open = txttoimageOpen;

  /**
   * Update the preferences in case they were changed
   */
  function txttoimagePrefs()
  {
    txttoimageDisabled           = txttoimagePrefsClass.getBoolPref('disabled');
    txttoimageImageLoader        = txttoimagePrefsClass.getBoolPref('imageLoader');
    txttoimageRegular            = [txttoimagePrefsClass.getBoolPref('Width'), txttoimagePrefsClass.getBoolPref('Height')];
    txttoimageRegularv           = [txttoimagePrefsClass.getIntPref('WidthValue'), txttoimagePrefsClass.getIntPref('HeightValue')];
    txttoimageImgmode            = txttoimagePrefsClass.getCharPref('mode');
    txttoimageMouse              = [txttoimagePrefsClass.getBoolPref('HoverW'), txttoimagePrefsClass.getBoolPref('HoverH')];
    txttoimageMousev             = [txttoimagePrefsClass.getIntPref('HoverWV'), txttoimagePrefsClass.getIntPref('HoverHV')];
    txttoimageBlacklist          = [];
    txttoimageSiteBlacklist      = [];
    txttoimageNoGameFAQsSig      = txttoimagePrefsClass.getBoolPref('noGameFAQsSig');
    txttoimageUndoOnDisable      = txttoimagePrefsClass.getBoolPref('undoOnDisable');
    var blackitems               = txttoimagePrefsClass.getCharPref('filter').split(' ');
    var blacksites               = txttoimagePrefsClass.getCharPref('siteFilter').split(' ');
    var regexpRegexp             = new RegExp('^/.*/[gim]*$', 'i');

    for (var i = 0; i < blackitems.length; i++) {
      try {
        // check to make sure there are blacklisted items
        if (!blackitems[i].length) {
          continue;
        }

        // check for use of regexp
        if (regexpRegexp.test(blackitems[i])) {
          var regexp = blackitems[i].substr(1, blackitems[i].length - 2);
        } else {
          // otherwise, remove special characters
          var regexp = blackitems[i].replace(/^[\*]*(.(?:[^\*]|[\*]+[^\*])*)[\*]*$/, '$1').replace(/[\*]+/, '*').replace(/([^\w\*])/g, '\\$1').replace(/\*/g, '.*') + '(?:\\n)?';
        }

        // add the blacklisted item
        txttoimageBlacklist.push(new RegExp(regexp, 'i'));
      } catch (e) {}
    }

    for (var i = 0; i < blacksites.length; i++) {
      try {
        // check to make sure there are blacklisted items
        if (!blacksites[i].length) {
          continue;
        }

        // check for use of regexp
        if (regexpRegexp.test(blacksites[i])) {
          var regexp = blacksites[i].substr(1, blacksites[i].length - 2);
        } else {
          // otherwise, remove special characters
          var regexp = blacksites[i].replace(/^[\*]*(.(?:[^\*]|[\*]+[^\*])*)[\*]*$/, '$1').replace(/[\*]+/, '*').replace(/([^\w\*])/g, '\\$1').replace(/\*/g, '.*') + '(?:\\n)?';
        }

        // add the blacklisted item
        txttoimageSiteBlacklist.push(new RegExp(regexp, 'i'));
      } catch (e) {}
    }

    var imageTypesRegExpString = txttoimagePrefsClass.getCharPref('imageTypes').replace(/^[ ]+/, '').replace(/[ ]+$/, '').replace(/[ ]+/g, '|').split('|').sort().reverse().join('|');
    txttoimageImgRegExp = new RegExp('^[^\\?]+?(?:\\.|%2E)(?:' + imageTypesRegExpString + ')$', 'i');
    txttoimageImgReplaceRegExp = new RegExp('^([-\\s\\(\\)\\[\\]\\{\\}=+_.!@#$%^&*<>?;\'"\\|/:a-z0-9]*?)(https?://[^\\?]+(?:\\.|%2E)(?:' + imageTypesRegExpString + '))([-\\s\\(\\)\\[\\]\\{\\}=+_.!@#$%^&*<>?;"\'\\|/:a-z0-9]*)$', 'i');
    txttoimageImgDetectRegExp = new RegExp('http[s]?://[^\\?]+(?:\\.|%2E)(?:' + imageTypesRegExpString + ')', 'i')
    // maximum number of images to display at any given time on a page
    txttoimageMaxImages = txttoimagePrefsClass.getIntPref('maxNumberOfImages');

    if (txttoimageMaxImages <= 0) {
      txttoimageMaxImages = 999999 // disables limit; I really doubt a single web page exists that has a million image links on it
    }
  }

  /**
   * Update the statusbar icon and set the event listener for the extension
   */
  function txttoimageCheckImageLoad(e)
  {
    if (txttoimageDisabled) {
      document.getElementById('txttoimagePanel').removeAttribute('disabled');
      setTimeout(txttoimageCheckDoc, 1, e.originalTarget);
    }
  }

  /**
   * Toggle the status bar icon status and set or remove event listeners
   */
  function txttoimageToggle()
  {
    txttoimageDisabled = !txttoimageDisabled;

    if (txttoimageDisabled) {
      txttoimagePrefsClass.setBoolPref('disabled', true);
      document.getElementById('txttoimagePanel').removeAttribute('disabled');
      setTimeout(txttoimageCheckDoc, 1, window.content.document);
    } else {
      txttoimagePrefsClass.setBoolPref('disabled', false);
      document.getElementById('txttoimagePanel').setAttribute('disabled', 'true');
      if (txttoimageUndoOnDisable)
        setTimeout(txttoimageUndo, 1);
    }
  }

  this.Toggle = txttoimageToggle;

  /**
   * Remove all images made by Text-to-Image in current window
   */
  function txttoimageUndo()
  {
    var imgs = window.content.document.images;
    for (var i = 0; i < imgs.length;)
    {
      if (imgs[i]._txttoimageData && imgs[i]._txttoimageData.isValid)
        imgs[i].parentNode.replaceChild(imgs[i]._txttoimageData.origNode, imgs[i]);
      else
        i++;
    }
  }

  /**
   * Sets the _txttoimageData object for images
   */
  function txttoimageData()
  {
    var data = {
      isValid   : false,
      isLoader  : false,
      origNode  : null,
      loaderImg : null
    };

    return data;
  }

  /**
   * The main function: replaces the links on the page to the images
   */
  function txttoimageCheckDoc(aDoc)
  {
    // don't do anything if the page isn't an actual (X)HTML or XML document
    if (!txttoimageValidDocTypes.test(aDoc.contentType)) {
      return;
    }

    // skip blacklisted sites
    for (var i = 0; i < txttoimageSiteBlacklist.length; i++) {
      if (txttoimageSiteBlacklist[i].test(aDoc.URL)) {
        return;
      }
    }

    try
    {
      var gBody = aDoc.body;
    }
    catch (e)
    {
      return;
    }

    txttoimageImgsonpage = 0;

    try
    {
      txttoimageNoGameFAQsSigCond = txttoimageNoGameFAQsSig && /(^|\.)gamefaqs\.com$/.test(aDoc.domain);
    }
    catch (e)
    { // for safety
      txttoimageNoGameFAQsSigCond = false;
    }

    var anchorNodes = gBody.getElementsByTagName('a');
    i = 0;
    while (i < anchorNodes.length && txttoimageImgsonpage < txttoimageMaxImages) {
      // if the link matches the image regexp and isn't blacklisted...
      if (txttoimageImgRegExp.test(anchorNodes[i].href) && txttoimageBlacklistPass(anchorNodes[i].href)) {
        txttoimageLink2Img(anchorNodes[i]);
        txttoimageImgsonpage++;
        // since we replace an anchor node, it gets removed dynamically from the HTML Collection variable
      } else {
        i++; // move on to the next item
      }
    }

    var textNodes = txttoimageGetTextNodes(gBody);
    i = 0;
    while (i < textNodes.length && txttoimageImgsonpage < txttoimageMaxImages) {
      txttoimageTxt2Img(textNodes[i]);
      i++;
    }

    txttoimageImgsonpage = 0;
  }

  this.CheckDoc = txttoimageCheckDoc;

  /**
   * Replace link with an image, assumes the link links to an image
   */
  function txttoimageLink2Img(aNode)
  {
    var gDoc         = aNode.ownerDocument;
    var gDimensions  = new Array(2);

    if (txttoimageRegular[0]) {
      gDimensions[0] = txttoimageRegularv[0] + 'px';
    } else {
      gDimensions[0] = 'auto';
    }

    if (txttoimageRegular[1]) {
      gDimensions[1] = txttoimageRegularv[1] + 'px';
    } else {
      gDimensions[1] = 'auto';
    }

    var img                      = gDoc.createElement('img');
    img._txttoimageData          = new txttoimageData();
    img._txttoimageData.isValid  = true;
    img._txttoimageData.origNode = aNode.cloneNode(1);
    img.src                      = aNode.href;
    img.className                = 'txttoimage_image';
    img.style.setProperty('max-width', gDimensions[0], 'important');
    img.style.setProperty('max-height', gDimensions[1], 'important');
    img.alt                      = aNode.textContent;
    img.title                    = aNode.textContent;

    // if the image is loading, use the image loader GIF; else, put the image in
    if (txttoimageImageLoader) {
      var loader                       = gDoc.createElement('img');
      loader._txttoimageData           = new txttoimageData();
      loader._txttoimageData.isValid   = true;
      loader._txttoimageData.isLoader  = true;
      loader._txttoimageData.loaderImg = img;
      img._txttoimageData.loadingImg   = loader;
      loader.src                       = txttoimageLoaderSrc;
      loader.className                 = 'txttoimage_loader';
      loader.title                     = txttoimageStrings.getString('loadingText');
      txttoimageStateChange(aNode.href, loader);
      aNode.parentNode.replaceChild(loader, aNode);
    } else {
      img.addEventListener('error', txttoimageBrokenImage, false);
      img.addEventListener('load', txttoimagePicFunc, false);
      aNode.parentNode.replaceChild(img, aNode);
    }
  }

  this.Link2Img = txttoimageLink2Img;

  /**
   * Replace text URL with image(s). Currently no way of limiting which URLs
   * are effected if there are multiple URLs in the same node
   */
  function txttoimageTxt2Img(aNode)
  {
    var gDimensions  = new Array(2);

    if (txttoimageRegular[0]) {
      gDimensions[0] = txttoimageRegularv[0] + 'px';
    } else {
      gDimensions[0] = 'auto';
    }

    if (txttoimageRegular[1]) {
      gDimensions[1] = txttoimageRegularv[1] + 'px';
    } else {
      gDimensions[1] = 'auto';
    }

    var gDoc = aNode.ownerDocument;
    var i    = 0;
    var j    = 0;
    // get individual words contained in text
    var split    = aNode.textContent.split(' ');
    var newNodes = [];

    // loop through words to check for textual links to images
    while (i < split.length && txttoimageImgsonpage < txttoimageMaxImages) {
      // we need the results from the regexp, not just whether the URI passes or not
      var match = split[i].match(txttoimageImgReplaceRegExp);

      // if the word is a text link to an image that passes the blacklist test, imagify it
      if (match && txttoimageBlacklistPass(match[2])) {
        var img                      = gDoc.createElement('img');
        img._txttoimageData          = new txttoimageData();
        img._txttoimageData.isValid  = true;
        img._txttoimageData.origNode = gDoc.createTextNode(match[2]);
        img.src                      = match[2];
        img.className                = 'txttoimage_image';
        img.style.setProperty('max-width', gDimensions[0], 'important');
        img.style.setProperty('max-height', gDimensions[1], 'important');
        img.alt                      = match[2];
        img.title                    = match[2];

        // if the image isn't loaded yet, put the image loader GIF in its place
        if (txttoimageImageLoader) {
          var loader                       = gDoc.createElement('img');
          loader._txttoimageData           = new txttoimageData();
          loader._txttoimageData.isValid   = true;
          loader._txttoimageData.isLoader  = true;
          loader._txttoimageData.loaderImg = img;
          img._txttoimageData.loadingImg   = loader;
          loader.src                       = txttoimageLoaderSrc;
          loader.className                 = 'txttoimage_loader';
          loader.title                     = txttoimageStrings.getString('loadingText');
          txttoimageStateChange(match[2], loader);
        } else {
          img.addEventListener('error', txttoimageBrokenImage, false);
          img.addEventListener('load', txttoimagePicFunc, false);
        }

        /*
         * create the three nodes for replacing; one for the text before the
         * URL, one for the image, and one for the text after the URL
         */
        if (match[1] != '' || match[3] != '') {
          var holdingArray = new Array(3);
          holdingArray[0]  = gDoc.createTextNode(match[1]);
          holdingArray[1]  = txttoimageImageLoader ? loader : img;
          holdingArray[2]  = gDoc.createTextNode(match[3]);
          newNodes[i]      = holdingArray;
        } else {
          newNodes[i] = img;
        }

        j++;
        txttoimageImgsonpage++;
      } else {
        newNodes[i] = gDoc.createTextNode(split[i]);
      }

      i++;
      // end loop for checking words
    }

    // TODO: too complicated and can easily run into an error
    var theParent = aNode.parentNode;

    if (j > 0 && split.length == 1) {
      var insertedNode = newNodes[0];

      if (!insertedNode.nodeName) {
        theParent.replaceChild(insertedNode[2], aNode);
        theParent.insertBefore(insertedNode[1], insertedNode[2]);
        theParent.insertBefore(insertedNode[0], insertedNode[1]);
      } else if (txttoimageImageLoader) {
        theParent.replaceChild(loader, aNode);
      } else {
        theParent.replaceChild(img, aNode);
      }
    } else if (j > 0) {
      var insertNode = newNodes[i - 1];
      var lastNode;

      if (!insertNode.nodeName) {
        theParent.replaceChild(insertNode[2], aNode);
        theParent.insertBefore(insertNode[1], insertNode[2]);
        theParent.insertBefore(insertNode[0], insertNode[1]);
        lastNode = insertNode[0];
      } else {
        theParent.replaceChild(insertNode, aNode);
        lastNode = insertNode;
      }

      var newLength = i;

      for (i = newLength - 2; i >= 0; i--) {
        var space        = gDoc.createTextNode(' ');
        var insertedNode = newNodes[i];

        if (!insertedNode.nodeName) {
          var filler = insertedNode[1];
          theParent.insertBefore(space, lastNode);
          theParent.insertBefore(insertedNode[2], space);
          theParent.insertBefore(insertedNode[1], insertedNode[2]);
          theParent.insertBefore(insertedNode[0], insertedNode[1]);
          lastNode = insertedNode[0];
        } else {
          theParent.insertBefore(space, lastNode);
          theParent.insertBefore(insertedNode, space);
          lastNode = insertedNode;
        }
      }
    }
  }

  this.Txt2Img = txttoimageTxt2Img;

  /**
   * Get all the text nodes that are descendents of <node>. Nodes that do not
   * meet certain conditions are filtered out.
   */
  function txttoimageGetTextNodes(node)
  {
    var textArray = [];
    var nodeName = node.nodeName.toLowerCase();
    if (nodeName != 'button'
        && nodeName != 'textarea'
        && nodeName != 'script')
    {
      textArray = txttoimageGetTextNodesRecursive(node);
    }
    return textArray;
  }

  /**
   * Get all the text nodes that are descendents of <node>. <node> should not
   * be a button, textarea, or script. Nodes that do not meet certain
   * conditions are filtered out.
   */
  function txttoimageGetTextNodesRecursive(node)
  {
    var children = node.childNodes;
    var textArray = [];

    for (var i = 0; i < children.length; i++)
    {
      var child = children[i];
      if (txttoimageNoGameFAQsSigCond && /^\s*---\s*$/.test(child.textContent))
        // If we see something that looks like a sig separator, ignore siblings
        // that follow. Though, this won't work if the separator is wrapped in
        // tags.
        break;
      var childNodeName = child.nodeName.toLowerCase();
      if (childNodeName == '#text')
      {
        if (txttoimageImgDetectRegExp.test(child.textContent))
          textArray.push(child);
      }
      else if (childNodeName != 'button'
          && childNodeName != 'textarea'
          && childNodeName != 'script'
          && txttoimageImgDetectRegExp.test(child.textContent)
          && child.hasChildNodes())
      {
        var textArray2 = txttoimageGetTextNodesRecursive(child);
        for (var j = 0; j < textArray2.length; j++)
          textArray.push(textArray2[j]);
      }
    }

    return textArray;
  }

  /**
   * Test if the URL passes all the blacklist filters
   */
  function txttoimageBlacklistPass(aUrl)
  {
    for (var i = 0; i < txttoimageBlacklist.length; i++) {
      try {
        // test the URL against the blacklist
        if (txttoimageBlacklist[i].test(aUrl)) {
          return false;
        }
      } catch(e) {}
    }

    return true;
  }

  function txttoimagePicFunc()
  {
    txttoimageSetEvents(this);
  }

  /**
   * Set all the extra functions after the image finishes loading. Plus, it will
   * replace the loader if loader images are activated
   */
  function txttoimageSetEvents(aImg)
  {
    if (!aImg.naturalWidth || !aImg.naturalHeight) {
      txttoimageBrokenImage({ currentTarget : aImg });
    }

    var gDimensions  = new Array(2);

    if (txttoimageRegular[0]) {
      gDimensions[0] = txttoimageRegularv[0] + 'px';
    } else {
      gDimensions[0] = 'auto';
    }

    if (txttoimageRegular[1]) {
      gDimensions[1] = txttoimageRegularv[1] + 'px';
    } else {
      gDimensions[1] = 'auto';
    }

    // hovering over large images (if enabled) will resize them according to preferences
    aImg.addEventListener('mouseover', txttoimagePicHover, false);
    aImg.addEventListener('mouseout', txttoimagePicOut, false);
    aImg.addEventListener('dblclick', txttoimageOpenWindow, true);

    // if image resizing is enabled, resize the image proportionally
    if (txttoimageRegular[0] && txttoimageRegular[1]) {
      var imgRatio = aImg.naturalHeight / aImg.naturalWidth;
      var maxRatio = txttoimageRegularv[1] / txttoimageRegularv[0];

      if (imgRatio >= maxRatio) {
        aImg.style.setProperty('max-width', (txttoimageRegularv[1] / imgRatio) + 'px', 'important');
        aImg.style.setProperty('max-height', gDimensions[1], 'important');
      } else {
        aImg.style.setProperty('max-width', gDimensions[0], 'important');
        aImg.style.setProperty('max-height', (txttoimageRegularv[0] * imgRatio) + 'px', 'important');
      }
    } else {
      aImg.style.setProperty('max-width', gDimensions[0], 'important');
      aImg.style.setProperty('max-height', gDimensions[1], 'important');
    }
  }

  /**
   * Replaces loader image with actual image after XMLHttpRequest finished
   */
  function txttoimageStateChange(aUrl, aNode)
  {
    var request                = new XMLHttpRequest();
    request.open('GET', aUrl);
    request._txttoimageData    = aNode;
    request.onreadystatechange = function()
    {
      if (request.readyState == 4 && request.status >= 200) {
        if (!request._txttoimageData.parentNode) {
          request._txttoimageData = request._txttoimageData._txttoimageData.loaderImg;
          txttoimageSetEvents(request._txttoimageData);
        } else {
          request._txttoimageData._txttoimageData.loaderImg.addEventListener('error', txttoimageBrokenImage, true);
          request._txttoimageData.parentNode.replaceChild(request._txttoimageData._txttoimageData.loaderImg, request._txttoimageData);

          if (request._txttoimageData._txttoimageData.loaderImg.complete) {
            txttoimageSetEvents(request._txttoimageData._txttoimageData.loaderImg);
          } else {
            request._txttoimageData._txttoimageData.loaderImg.addEventListener('load', txttoimagePicFunc, true);
          }
        }
      }
    };

    request.send(null);
  }

  /**
   * if we're over an image made by Text-to-Image, display some special items in
   * the context menu
   */
  function txttoimagePopup()
  {
    txttoimageImgsonpage = 0;
    var img              = gContextMenu.target._txttoimageData;
    var hide             = (!img || !img.isValid);
    var link2img         = (!txttoimageImgRegExp.test(gContextMenu.linkURL));

    var sep1 = document.getElementById('txttoimageSeparatorOne');
    var sep2 = document.getElementById('txttoimageSeparatorTwo');

    if (!sep1.previousSibling || sep1.previousSibling.tagName.toLowerCase() == 'menuseparator') {
      sep1.hidden = true;
    } else {
      sep1.hidden = hide;
    }

    document.getElementById('txttoimageOpen').hidden      = hide;
    document.getElementById('txttoimageRemove').hidden    = hide;
    document.getElementById('txttoimageBlacklist').hidden = hide;
    // TODO: make this function work
    //document.getElementById('txttoimageTxt-Img').hidden   = hide;
    document.getElementById('txttoimageLink-Img').hidden  = link2img;

    if (!sep2.nextSibling || sep2.nextSibling.tagName.toLowerCase() == 'menuseparator') {
      sep2.hidden = true;
    } else {
      sep2.hidden = hide;
    }
  }

  /**
   * Open the add filter dialog
   */
  this.AddFilter = function(aUrl, aType)
  {
    window.openDialog('chrome://txttoimage/content/addFilter.xul', txttoimageStrings.getString('addFilterText'), 'chrome, modal, centerscreen, dialog', aUrl, aType);
  }

  /**
   * Open a window to view the image in at full size
   */
  function txttoimageOpenWindow(e)
  {
    if (txttoimageImgmode == 'new') {
      window.openDialog('chrome://txttoimage/content/view.xhtml', '', 'chrome, resizable, location=no, menubar=no, directories=no, toolbar=no, status=no', e.target.src);
    }
  }

  this.OpenWindow = txttoimageOpenWindow;

  /**
   * resize the image on hover over and other stuff
   */
  function txttoimagePicHover(e)
  {
    var dimensions = new Array(2);
    var img        = e.target;
    txttoimageCurrImg = img;

    if (txttoimageMouse[0]) {
      dimensions[0] = txttoimageMousev[0] + 'px';
    } else {
      dimensions[0] = 'auto';
    }

    if (txttoimageMouse[1]) {
      dimensions[1] = txttoimageMousev[1] + 'px';
    } else {
      dimensions[1] = 'auto';
    }

    if (txttoimageMouse[0] && txttoimageMouse[1]) {
      var imgRatio = img.naturalHeight / img.naturalWidth;
      var maxRatio = txttoimageMousev[1] / txttoimageMousev[0];

      if (imgRatio >= maxRatio) {
        img.style.setProperty('max-width', (txttoimageMousev[1] / imgRatio) + 'px', 'important');
        img.style.setProperty('max-height', dimensions[1], 'important');
      } else {
        img.style.setProperty('max-width', dimensions[0], 'important');
        img.style.setProperty('max-height', (txttoimageMousev[0] * imgRatio) + 'px', 'important');
      }
    } else {
      img.style.setProperty('max-width', dimensions[0], 'important');
      img.style.setProperty('max-height', dimensions[1], 'important');
    }

    if (txttoimageImgmode == 'new') {
      // for Firefox 1.4 and up, window.status just won't cut it for some reason
      window.content.document.defaultView.status = txttoimageStrings.getString('dblClick');
      img.style.setProperty('cursor', 'pointer', 'important');
    } else {
      img.style.removeProperty('cursor');
    }
  }

  /**
   * undoes anything the previous function did
   */
  function txttoimagePicOut(e)
  {
    txttoimageCurrImg = null;
    var dimensions    = new Array(2)
    var img           = e.target;

    if (txttoimageRegular[0]) {
      dimensions[0] = txttoimageRegularv[0] + 'px';
    } else {
      dimensions[0] = 'auto';
    }

    if (txttoimageRegular[1]) {
      dimensions[1] = txttoimageRegularv[1] + 'px';
    } else {
      dimensions[1] = 'auto';
    }

    if (txttoimageRegular[0] && txttoimageRegular[1]) {
      var imgRatio = img.naturalHeight / img.naturalWidth;
      var maxRatio = txttoimageRegularv[1] / txttoimageRegularv[0];

      if (imgRatio >= maxRatio) {
        img.style.setProperty('max-width', (txttoimageRegularv[1] / imgRatio) + 'px', 'important');
        img.style.setProperty('max-height', dimensions[1], 'important');
      } else {
        img.style.setProperty('max-width', dimensions[0], 'important');
        img.style.setProperty('max-height', (txttoimageRegularv[0] * imgRatio) + 'px', 'important');
      }
    } else {
      img.style.setProperty('max-width', dimensions[0], 'important');
      img.style.setProperty('max-height', dimensions[1], 'important');
    }

    window.content.document.defaultView.status = '';
  }

  /**
   * remove the targeted image; for the context menu only
   */
  this.RemoveImage = function(gCM)
  {
    var img      = gCM.target;
    var origNode = img._txttoimageData.origNode;

    if (!origNode) {
      img.parentNode.removeChild(img);
      return;
    }

    img.parentNode.replaceChild(origNode, img);
  }

  /**
   * Removes the image if it was a bad link and replaces it with its original
   * text/link
   */
  function txttoimageBrokenImage(e)
  {
    var img      = e.currentTarget;
    var origNode = img._txttoimageData.origNode;

    if (!origNode) {
      img.parentNode.removeChild(img);
      return;
    }

    img.parentNode.replaceChild(origNode, img);
  }

  /**
   * Fixes the bug with the image still staying at the hover-over size when
   * opening it in a pop-up by double-clicking it
   */
  function txttoimageWindowBlur()
  {
    if (txttoimageCurrImg) {
      txttoimagePicOut({ target : txttoimageCurrImg });
    }
  }
};

window.addEventListener('load', TxttoImage.Init, false);
