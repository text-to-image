var TTIview =
{
  lastX: 0,
  lastY: 0,

  onload: function()
  {
    img = document.createElement('img');

    img.addEventListener('load', function()
    {
      window.moveTo(window.screen.availLeft, window.screen.availTop);

      // sizeToContent is not reliable, so instead of doing that and then
      // resizing the window if it is bigger than the screen, we have to do all
      // of this.

      // We want to get the window border to be a stable size by making the
      // window as large as we would ever want to set it. The border should
      // never get any smaller than this because the scrollbars aren't going to
      // go away if we shrink the window, nor vice versa because we'll ensure
      // that we won't shrink the window too much.
      window.resizeTo(window.screen.availWidth, window.screen.availHeight);

      // Now we calculate the size the window should really be at...
      var w = this.naturalWidth + window.outerWidth - this.parentNode.clientWidth;
      var h = this.naturalHeight + window.outerHeight - this.parentNode.clientHeight;

      // And make sure we aren't making it any larger...
      if (w > window.screen.availWidth)
        w = window.screen.availWidth;
      if (h > window.screen.availHeight)
        h = window.screen.availHeight;

      window.resizeTo(w, h);
    }, false);

    img.src = window.arguments[0];

    document.getElementById('center').appendChild(img);

    document.body.addEventListener('dblclick', function()
    {
      window.close();
    }, false);

    document.body.style.setProperty('cursor', '-moz-grab', 'important');

    document.body.addEventListener('mousedown', function(e)
    {
      document.body.style.setProperty('cursor', '-moz-grabbing', 'important');
      document.body.addEventListener('mousemove', TTIview.mouseScroll, false);
      TTIview.setPos(e);
    }, false);

    document.body.addEventListener('mouseup', function(e)
    {
      document.body.style.setProperty('cursor', '-moz-grab', 'important');
      document.body.removeEventListener('mousemove', TTIview.mouseScroll, false);
    }, false);
  },

  mouseScroll: function(e)
  {
    window.scrollBy(TTIview.lastX - e.pageX, TTIview.lastY - e.pageY);
    TTIview.setPos(e);
  },

  setPos: function(e)
  {
    TTIview.lastX = e.pageX;
    TTIview.lastY = e.pageY;
  }
};

window.addEventListener('load', TTIview.onload, false);
