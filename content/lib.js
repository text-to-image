var TTIlib =
{
  version: Cc['@mozilla.org/extensions/manager;1']
    .getService(Ci.nsIExtensionManager)
    .getItemForID('{f701c26a-479a-4724-b4f1-870db12f063c}')
    .version,

  prefs: Cc['@mozilla.org/preferences-service;1']
    .getService(Ci.nsIPrefService)
    .getBranch('txttoimage.'),

  log: function(msg)
  {
    Cc['@mozilla.org/consoleservice;1']
      .getService(Ci.nsIConsoleService)
      .logStringMessage('Text-to-Image: ' + msg);
  },

  alert: function(msg)
  {
    Cc['@mozilla.org/embedcomp/prompt-service;1']
      .getService(Ci.nsIPromptService)
      .alert(null, 'Text-to-Image', msg);
  },

  confirm: function(msg)
  {
    return Cc['@mozilla.org/embedcomp/prompt-service;1']
      .getService(Ci.nsIPromptService)
      .confirm(null, 'Text-to-Image', msg);
  }
};
